from django.shortcuts import render


from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from rest_framework_simplejwt.tokens import AccessToken, RefreshToken
from django.contrib.auth.decorators import login_required
# from rest_framework.decorators import detail_route, list_route
from rest_framework.views import *
from rest_framework import viewsets
from rest_framework.permissions import *
from rest_framework.authentication import TokenAuthentication, BasicAuthentication, SessionAuthentication
from rest_framework.response import Response
from django.db.models import Q
import hashlib, random
from .models import *
from .serializers import *
from django.contrib import auth
# from rest_framework_jwt.settings import api_settings
from rest_framework_simplejwt.authentication import JWTTokenUserAuthentication

def get_tokens_for_user(user):
    refresh = RefreshToken.for_user(user)

    return {
        'refresh_token': str(refresh),
        'access_token': str(refresh.access_token),
    }


class LoginView(APIView):
    permission_classes = (AllowAny,)
    queryset = UserModel.objects.all()

    def post(self, request, *args, **kwargs):
        response = {}
        username = request.data['email']
        # email = request.data['email']
        password = request.data['password']
        hashed_password = hashlib.md5(password.encode()).hexdigest()
        print(username)
        if UserModel.objects.filter(email = username).exists():
            user_object = UserModel.objects.get(email = username)
            print(user_object)
            if user_object is not None:
                if PasswordModel.objects.filter(password = hashed_password).exists():
                    tokenr = TokenObtainPairSerializer()
                    tokenr.username = username
                    tokenr.password = password
                    refresh_token = tokenr.get_token(user_object)
                    access_token = refresh_token.access_token
                    user = UserSerializer(user_object, many=False).data
                    # print(user)
                else:
                    response['result'] = 0
                    response['message'] = "Password is incorrect"
                    return Response(response, status=401)
            else:
                response['result'] = 0
                response['message'] = "User with these credentials doesn't exists"
                return Response(response, status=401)                    
        else:
            response['result'] = 0
            response['message'] = "User with these credentials doesn't exists"
            return Response(response, status=401)

        return Response({'token': access_token['jti'], 'refresh_token':refresh_token['jti'],  'user': user}, status=200)

class RegisterView(APIView):
    permission_classes = (AllowAny,)
    queryset = UserModel.objects.all()

    def post(self, request, *args, **kwargs):
        response = {}
        if request.data['firstName']:
            first_name = request.data['firstName']
        if request.data['lastName']:
            last_name = request.data['lastName']
        if request.data['username']:
            username = request.data['username']
        if request.data['password']:
            password = request.data['password']
        if request.data['email']:
            email = request.data['email']
        if request.data['designation']:
            designation = request.data['designation']
        if request.data['userTypeChoice']:
            user_type_choice = request.data['userTypeChoice']
        if not UserModel.objects.filter(email = email).exists():
            platform_user = UserModel(first_name= first_name, last_name = last_name, username = username, email =email, designation = designation, user_type_choice = user_type_choice, signup_complete = True)
            user_password = PasswordModel(password_open = password)
            user_password.save()
            platform_user.save()
            user_password.user = platform_user
            user_password.save()
        else:
            response['result'] = 0
            response['message'] = "User with this email address already present. Please try any another email address."
            return Response(response, status=400)
        response['message'] = "You've been registered Successfully!!"
        response['result'] = 1
        return Response(response, status=200)


class LogoutView(APIView):
    permission_classes = (AllowAny,)
    authentication_classes = (JWTTokenUserAuthentication,)
    queryset = UserModel.objects.all()

    def post(self, request, *args, **kwargs):
        response = {}
        if request.data['token']:
            print("vjnfjvnfvj")
        else:
            response['result'] = 0
            response['message'] = "Invalid Token"
            return Response(response, status=400)
        response['message'] = "You've been logged out Successfully!!"
        response['result'] = 1
        return Response(response, status=200)

# class UserLoginViewSet(viewsets.ViewSet):
#     permission_classes = (AllowAny,)
#     authentication_classes = ( BasicAuthentication,)
#     @list_route(methods=['post'])
#     def login(self, request):
#         print("login API CAlled")
#         response = {}
#         username = request.data['username']
#         # email = request.data['email']
#         password = request.data['password']
#         #|Q(email = email)
#         hashed_password = hashlib.md5(password.encode()).hexdigest()
#         if UserModel.objects.filter(username = username).exists():
#             user_object = UserModel.objects.get(username = username)
#             # user_object = list(set(user_list))
#             if PasswordModel.objects.filter(password = hashed_password).exists():
#                 user = UserSerializer(user_object, many=False).data
#                 token = RefreshToken.for_user(user)
#             else:
#                 response['result'] = 0
#                 response['message'] = "Password is incorrect"
#                 return JsonResponse(response)
#         else:
#             response['result'] = 0
#             response['message'] = "User with these credentials doesn't exists"
#             return JsonResponse(response)

#         return JsonResponse({'token': token, 'user': user})