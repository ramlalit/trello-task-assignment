from django.db import models
from django.contrib.auth.models import User
import os
import random
import datetime
import hashlib
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
# Create your models here.

class UserModel(User):
    USER_TYPE_CHOICES = (
        ('Individual', 'Individual'),
        ('Team', 'Team'),
    )
    name = models.CharField(blank = True, null = True, default = '', max_length = 255)
    mobile = models.CharField(blank = True, null = True, default = '', max_length = 255)
    designation = models.CharField(blank = True, null = True, default = '', max_length = 255)
    user_type_choice = models.CharField(choices = USER_TYPE_CHOICES, max_length = 255, default='', blank = True, null = True)
    phone_verified = models.BooleanField(default = False)
    email_verified = models.BooleanField(default = False)
    signup_complete = models.BooleanField(default = False)
    is_deleted = models.BooleanField(default = False)
    created_at = models.DateTimeField(editable = False)
    updated_at = models.DateTimeField()
    def __str__(self):
        return str(self.username) + " " + str(self.pk)
    def save(self, *args, **kwargs):
        if not self.id:
            self.created_at = datetime.datetime.now()
        self.updated_at = datetime.datetime.now()
        return super(UserModel, self).save(*args, **kwargs)
    class Meta:
        verbose_name_plural = "Platform User"

class PasswordModel(models.Model):
    password_open = models.CharField(blank = True, null = True, default = '', max_length = 255)
    password = models.CharField(blank = True, null = True, default = '', max_length = 255)
    user = models.ForeignKey(UserModel, blank=True, null=True, on_delete=models.CASCADE)
    created_at = models.DateTimeField(editable = False)
    updated_at = models.DateTimeField()
    def __str__(self):
        return str(self.user.username)
    def save(self, *args, **kwargs):
        if not self.id:
            self.created_at = datetime.datetime.now()
        self.updated_at = datetime.datetime.now()
        self.password = hashlib.md5(self.password_open.encode()).hexdigest()

        super(PasswordModel, self).save(*args, **kwargs)
    class Meta:
        verbose_name_plural = "User Passwords"
