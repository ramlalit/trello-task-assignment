from django.contrib import admin
from .models import *
# Register your models here.
class UserAdmin(admin.ModelAdmin):
    # pass
    list_display = ['pk', 'username', 'name', 'email', 'mobile', 'designation', 'user_type_choice']
    list_filter = ('designation', 'user_type_choice')
    search_fields = ('name', 'email', 'username', 'mobile')
    ordering = ('-created_at',)
class PasswordModelAdmin(admin.ModelAdmin):
    # pass
    list_display = ['password_open', 'user', 'password',]
    # list_filter = ('designation', 'user_type_choice')
    search_fields = ('user',)
    ordering = ('-created_at',)
admin.site.register(UserModel, UserAdmin)
admin.site.register(PasswordModel, PasswordModelAdmin)
