"""ufaberAssignment URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include, re_path

from customuser.views import *
from management.views import *
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
    TokenVerifyView,
)
# from users.views import ActivateUser
from rest_framework import routers

router = routers.DefaultRouter()
# router.register(r'^api/other/', UserLoginViewSet, basename="login_api")
router.register(r'project', ProjectViews, "project")
router.register(r'task', TaskViews, "task")


urlpatterns = [
    path('admin/', admin.site.urls),
    re_path(r'^api/token/$', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    re_path(r'^api/token/refresh/$', TokenRefreshView.as_view(), name='token_refresh'),
    re_path(r'^api/token/verify/$', TokenVerifyView.as_view(), name='token_verify'),
    re_path(r'^api/login/$', LoginView.as_view(), name ="login"),
    re_path(r'^api/logout/$', LogoutView.as_view(), name ="logout"),
    re_path(r'^api/register/$', RegisterView.as_view(), name ="register"),
    re_path(r'^api/', include(router.urls))
]
