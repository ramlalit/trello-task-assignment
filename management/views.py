from django.shortcuts import render
from .models import *
# Create your views here.
from rest_framework.decorators import action
from rest_framework.views import *
from rest_framework import viewsets
from rest_framework.permissions import *
from rest_framework.authentication import TokenAuthentication, BasicAuthentication, SessionAuthentication
from rest_framework.response import Response
from django.db.models import Q
import hashlib, random
from .models import *
from .serializers import *
from django.contrib import auth

class ProjectViews(viewsets.ViewSet):
    serializer_class = ProjectSerializer
    @action(detail=False, methods=['POST'])
    def create_project(self, request):
        # self.queryset = Project.objects.all()
        response = {}
        project_name = request.data['projectName']
        project_id = request.data['projectId']
        user_id = request.data['userId']
        if project_name:
            if project_id:
                project = Project.objects.get(pk = int(project_id), user__id = user_id)
            else:
                user = UserModel.objects.get(pk = int(user_id))
                project = Project(name = project_name, user = user)
                project.created_at = datetime.datetime.now()
            
            project.name=project_name
            if "projectDescription" in request.data:
                project.description=request.data['projectDescription']
            if "projectImage" in request.data:
                project.project_type=request.data['projectImage']
            project.save()
            response['data'] = self.serializer_class(Project.objects.filter(user__id=user_id, isDeleted=False), many = True).data
        else:
            response['message'] = "Unauthorized Access."
            response['result'] = 0
            return Response(response, status = 400)

        return Response(response, status = 200)

    @action(detail=False, methods=['POST'])
    def get_project_by_id(self, request):
        self.queryset = Project.objects.all()
        response = {}

        if "projectId" in request.data:
            projectId = request.data['projectId']
            if self.queryset.filter(pk=projectId).exists():
                project = self.queryset.get(pk=projectId)
                project = self.serializer_class(project, many=False).data
                response['data'] = project
                response['result'] = 1
            else:
                response['message'] = "No such Project found"
                response['result'] = 0
                return Response(response, status = 400)
        else:
            response['message'] = "Please specify Project ID"
            response['result'] = 0
            return Response(response, status = 400)
        
        return Response(response, status = 200)

    @action(detail=False, methods=['POST'])
    def get_project_by_name(self, request):
        self.queryset = Project.objects.all()
        response = {}
        if request.data['projectName']:
            projectName = request.data['projectName']
            if self.queryset.filter(name = projectName).exists():
                project = self.queryset.filter(name = projectName).first()
                project = self.serializer_class(project, many=False).data
                response['data'] = project
                response['result'] = 1
            else:
                response['message'] = "No such Project found"
                response['result'] = 0
                return Response(response, status = 400)
        else:
            response['message'] = "Please specify Project Name."
            response['result'] = 0
            return Response(response, status = 400)
        return Response(response, status = 200)

    @action(detail=False, methods=['POST'])
    def delete_project(self, request):
        self.queryset = Project.objects.all()
        response = {}
        userId = request.data['userId']
        if UserModel.objects.filter(pk = int(userId)).exists():
            # user = UserModel.objects.get(pk = int(userId))
            if request.data['projectId']:
                project_id = request.data['projectId']
                if self.queryset.filter(pk = int(project_id)).exists():
                    project = self.queryset.get(pk = int(project_id))
                    project.isDeleted=True
                    project.save()
                    projects = self.serializer_class(Project.objects.filter(user__id=userId, isDeleted=False), many=True).data
                    response['data'] = projects
                    response['result'] = 1
                else:
                    response['message'] = "Unauthorized Access."
                    response['result'] = 0
                    return Response(response, status = 400)
            else:
                response['message'] = "You haven't selected a Project to remove."
                response['result'] = 0
                return Response(response, status = 400)
        else:
            response['message'] = "Unauthorized Access."
            response['result'] = 0
            return Response(response, status = 400)
        return Response(response, status = 200)


    @action(detail=False, methods=['GET'])
    def get_projects(self, request):
        # self.queryset = Project.objects.all()
        response = {}
        if 'userId' in request.GET:
            userId = request.GET['userId']
            response['data'] = self.serializer_class(Project.objects.filter(user__id = request.GET['userId'], isDeleted=False), many = True).data
            response['result'] = 1
        else:
            response['message'] = "No Projects are available."
            response['result'] = 0
            return Response(response, status = 400)
        return Response(response, status = 200)

class TaskViews(viewsets.ViewSet):
    queryset = ProjectTask.objects.all()
    serializer_class = ProjectTaskSerializer
    @action(detail=False, methods=['POST'])
    def get_task_by_project_id(self, request):
        response = {}
        userId = request.data['userId']
        if UserModel.objects.filter(pk = int(userId)).exists():
            # user = UserModel.objects.get(pk = int(userId))
            if 'projectId' in request.data:
                project_id = request.data['projectId']
                queryset = queryset.objects.filter(project__id=project_id, user_assigned__id=userId, isDeleted=False)
                response['data'] = self.serializer_class(queryset, many = True).data
                response['result'] = 1
            else:
                response['message'] = "Please select valid project"
                response['result'] = 0
                return Response(response, status = 400)
        else:
            response['message'] = "Unauthorized Access."
            response['result'] = 0
            return Response(response, status = 400)
        return Response(response, status = 200)

    @action(detail=False, methods=['POST'])
    def get_task_by_name(self, request):
        response = {}
        return Response(response, status = 200)

    @action(detail=False, methods=['POST'])
    def create_task(self, request):
        response = {}
        task_name = request.data['taskName']
        task_id = request.data['taskId']
        if "subtaskId" in request.data:
            subtask_id = request.data['subtaskId']
        user_id = int(request.data['userId'])
        if task_name:
            if UserModel.objects.filter(pk = user_id).exists():
                user = UserModel.objects.get(pk = user_id)
                if "projectId" in request.data:
                    project_id = int(request.data['projectId'])
                    if Project.objects.filter(pk = project_id).exists():
                        project = Project.objects.get(pk = project_id)
                        if task_id:
                            task = ProjectTask.objects.get(pk = task_id)

                            if "subtaskId" in request.data:
                                if subtask_id:
                                    task = ProjectTask.objects.get(pk = subtask_id, parent_task_id=task_id)
                                else:
                                    task = ProjectTask(user_assigned = user, project=project)
                                    task.created_at = datetime.datetime.now()
                        else:
                            task = ProjectTask(user_assigned = user, project=project)
                            task.created_at = datetime.datetime.now()
                else:
                    if task_id:
                        task = ProjectTask.objects.get(pk = task_id)
                        project_id=task.project.id

                        if "subtaskId" in request.data:
                            if subtask_id:
                                task = ProjectTask.objects.get(pk = subtask_id, parent_task_id=task_id)
                            else:
                                project = Project.objects.get(pk = task.project.id)
                                task = ProjectTask(user_assigned = user, project=project)
                                task.created_at = datetime.datetime.now()
                                task.parent_task_id=task_id
                task.name = task_name
                
                if "taskDescription" in request.data:
                    task.description = request.data["taskDescription"]
                if "TaskImage" in request.data:
                    task.image_url = request.data["TaskImage"]
                task.save()
                
                self.queryset = ProjectTask.objects.filter(user_assigned__id=user_id, project__id=project_id, isDeleted=False)
                response['data'] = self.serializer_class(self.queryset, many = True).data
                response['result'] = 1
            else:
                response['message'] = "Unauthorized Access."
                response['result'] = 0
                return Response(response, status = 400)
        else:
            response['message'] = ["Please specify a name to the Task."]
            response['result'] = 0
            return Response(response, status = 400)

        return Response(response, status = 200)

    @action(detail=False, methods=['POST'])
    def delete_task(self, request):
        # self.queryset = ProjectTask.objects.all()
        response = {}
        userId = request.data['userId']
        project_id = request.data['projectId']
        if UserModel.objects.filter(pk = int(userId)).exists():
            # user = UserModel.objects.get(pk = int(userId))
            if "taskId" in  request.data:
                task_id = request.data['taskId']
                if ProjectTask.objects.filter(pk = int(task_id), project__pk = int(project_id), user_assigned__id=userId).exists():
                    task = self.queryset.get(pk = int(task_id), project__pk = int(project_id), user_assigned__id=userId)
                    task.isDeleted=True
                    self.queryset = ProjectTask.objects.filter(user_assigned__id=userId, project__id=project_id, isDeleted=False)
                    tasks = self.serializer_class(self.queryset, many=True).data
                    response['data'] = tasks
                    response['result'] = 1
                else:
                    response['message'] = "Unauthorized Access."
                    response['result'] = 0
                    return Response(response, status = 400)
            else:
                response['message'] = "You haven't selected a Task to remove."
                response['result'] = 0
                return Response(response, status = 400)
        else:
            response['message'] = "Unauthorized Access."
            response['result'] = 0
            return Response(response, status = 400)
        return Response(response, status = 200)