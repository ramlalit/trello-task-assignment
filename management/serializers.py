from rest_framework import serializers
from .models import *
from customuser.serializers import *


class ProjectLinkedSerializer(serializers.ModelSerializer):

    class Meta:
        model = Project
        fields = ('id', 'name')

class ProjectTaskLinkedSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Project
        fields = ('id', 'name')

class TaskCommentsSerializer(serializers.ModelSerializer):
    user_assigned = UserSerializer(many = False)
    task = ProjectTaskLinkedSerializer(many = False)

    class Meta:
        model = Comments
        fields = ('id', 'user_assigned', 'task', 'description', 'isDeleted')

class ProjectTaskSerializer(serializers.ModelSerializer):
    user_assigned = UserSerializer(many = False)
    project = ProjectLinkedSerializer(many = False)
    subtaskList = serializers.SerializerMethodField('get_subtasks')

    def get_subtasks(self, obj):
        return ProjectTaskSerializer(ProjectTask.objects.filter(parent_task_id=obj.id, parent_task_id__isnull=False, isDeleted=False), many= True).data

    class Meta:
        model = ProjectTask
        fields = ('id', 'user_assigned', 'subtaskList', 'project', 'name',  'start_at', 'end_at', 'description', 'image_url', 'isClosed', 'isDeleted')

class ProjectSerializer(serializers.ModelSerializer):
    user = UserSerializer(many = False)
    taskList = serializers.SerializerMethodField('get_tasks')

    def get_tasks(self, obj):
        return ProjectTaskSerializer(ProjectTask.objects.filter(project__id = obj.id, parent_task_id__isnull=True, isDeleted=False), many= True).data
    class Meta:
        model = Project
        fields = ('id', 'user', 'taskList', 'name', 'start_at', 'end_at', 'description', 'image_url', 'isArchived', 'isClosed', 'isDeleted')

