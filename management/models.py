from django.db import models
import os
import random
import datetime
import hashlib
from customuser.models import *
from django.shortcuts import get_object_or_404
from django.http import Http404
from django.utils import timezone


# Create your models here.
class Project(models.Model):
    name = models.CharField(blank = False, null = False, default = '', max_length = 255, unique = True)
    project_type = models.CharField(blank = True, null = True, default = '', max_length = 255)
    description = models.CharField(blank = True, null = True, default = '', max_length = 255)
    start_at = models.DateTimeField(blank=True, null=True)
    end_at = models.DateTimeField(blank=True, null=True)
    image_url = models.CharField(blank = True, null = True, default = '', max_length = 255)
    user = models.ForeignKey(UserModel, blank=True, null=True, on_delete=models.CASCADE)    
    isDone = models.BooleanField(default = False)
    isClosed = models.BooleanField(default = False)
    isDeleted = models.BooleanField(default = False)
    isArchived = models.BooleanField(default = False)
    created_at = models.DateTimeField(editable = False)
    updated_at = models.DateTimeField()
    
    def __str__(self):
        return self.name
    
    def save(self, *args, **kwargs):
        if not self.created_at:
            self.created_at = datetime.datetime.now()
        self.updated_at = datetime.datetime.now()
        return super(Project, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = "Project List"

class ProjectTask(models.Model):
    name = models.CharField(blank = False, null = False, default = '', max_length = 255, unique = True)
    project = models.ForeignKey(Project, blank=True, null=True, on_delete=models.CASCADE)
    description = models.CharField(blank = True, null = True, default = '', max_length = 255)
    start_at = models.DateTimeField(blank=True, null=True)
    end_at = models.DateTimeField(blank=True, null=True)
    image_url = models.CharField(blank = True, null = True, default = '', max_length = 255)
    parent_task_id = models.IntegerField(blank=True, null=True)
    user_assigned = models.ForeignKey(UserModel, blank=True, null=True, on_delete=models.CASCADE)    
    isDone = models.BooleanField(default = False)
    isClosed = models.BooleanField(default = False)
    isDeleted = models.BooleanField(default = False)
    created_at = models.DateTimeField(editable = False)
    updated_at = models.DateTimeField()
    
    def save(self, *args, **kwargs):
        if not self.created_at:
            self.created_at = datetime.datetime.now()
        self.updated_at = datetime.datetime.now()
        return super(ProjectTask, self).save(*args, **kwargs)
    
    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = "Task List"

class Comments(models.Model):
    task = models.ForeignKey(ProjectTask, blank=True, null=True, on_delete=models.CASCADE)
    description = models.TextField(blank = True, null = True, default = '')
    user_assigned = models.ForeignKey(UserModel, blank=True, null=True, on_delete=models.CASCADE)
    isDeleted = models.BooleanField(default = False)
    created_at = models.DateTimeField(editable = False)
    updated_at = models.DateTimeField()
    
    def __str__(self):
        return self.task.name
    
    class Meta:
        verbose_name_plural = "Task Comments List"