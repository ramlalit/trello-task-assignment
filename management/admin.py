from django.contrib import admin
from .models import Project
from .models import ProjectTask
# Register your models here.
class ProjectAdmin(admin.ModelAdmin):
    list_display = ['pk', 'name', 'user']

class ProjectTaskAdmin(admin.ModelAdmin):
    list_display = ['pk', 'name', 'project', 'user_assigned']

admin.site.register(Project, ProjectAdmin)
admin.site.register(ProjectTask, ProjectTaskAdmin)